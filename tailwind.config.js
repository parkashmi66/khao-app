module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        blue: '#295ACC',
        green:"#86ba90",
      }
    }
  },
  plugins: [],
}