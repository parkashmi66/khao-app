export default function Billcomponent() {
  return (
    <div>
      <div className="container mx-auto p-44">
        <div className="shadow-xl boder p-24 flex flex-col items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-12 w-12 "
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
              clipRule="evenodd"
            />
          </svg>
          <div className="flex flex-col items-center py-4">
            <h1 className="text-3xl animate-bounce">Thankyou</h1>
            <h1 className="text-3xl animate-bounce">Order Success</h1>
          </div>
        </div>
        {/* <table className="shadow-lg bg-white w-full">
          <tr>
            <th className="bg-green border text-center px-8 py-4">Id</th>
            <th className="bg-green border text-center px-8 py-4">Description</th>
            <th className="bg-green border text-center px-8 py-4">Qty</th>
            <th className="bg-green border text-center px-8 py-4">Unit Cost</th>
            <th className="bg-green border text-center px-8 py-4">Total</th>
          </tr>
          <tbody>
            <tr>
              <td className="text-center border">1</td>
              <td className="text-center border">Burger</td>
              <td className="text-center border">5</td>
              <td className="text-center border">54</td>
              <td className="text-center border">87</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="5" className="text-3xl flex justify-center">
                Total Bill Amount
              </td>
              <td className="text-4xl">Rs.45546</td>
            </tr>
          </tfoot>
        </table> */}
      </div>
    </div>
  );
}
