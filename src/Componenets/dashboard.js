import {Route, Routes } from "react-router-dom";
import Header from "../Common/header";
import Sidebar from "../Common/sidebar";
import Billcomponent from "./billComponent";
import FoodOrder from "./foodOrder";
import Stats from "./stats";


export default function Dashboard() {
  return (
    <div>
      <div className="w-full mx-auto bg-green lg:p-4">
        <div className="flex">
          <div className="lg:flex hidden">
            <div className="bg-green h-screen">
              <div className="w-56">
                <div className="lg:ml-4">
                  <div className="flex">
                    <Sidebar/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full h-auto">
            <div className="bg-gray-100 h-full rounded-lg">
              <Header />
              {/* <Stats/> */}
              <Routes>
                <Route path="foodOrder" element={<FoodOrder/>}/>
                <Route path="stats" element={<Stats/>}/>
                <Route path="billComponent" element={<Billcomponent/>}/>
              </Routes>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
