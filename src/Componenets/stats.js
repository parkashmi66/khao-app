export default function Stats() {
  return (
    <div>
      <div className="p-12 flex justify-center">
        <div className="w-full border-2 shadow-xl">
          <div className="grid grid-cols-3 p-24 gap-8">
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Total Sales</h1>
              <h1 className="text-white text-4xl py-8">Rs.22000</h1>
            </div>
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Card Total</h1>
              <h1 className="text-white text-4xl py-8">Rs.21000</h1>
            </div>
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Cash total</h1>
              <h1 className="text-white text-4xl py-8">Rs.50,000</h1>
            </div>
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Total Orders</h1>
              <h1 className="text-white text-4xl py-8">564</h1>
            </div>
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Total delivery</h1>
              <h1 className="text-white text-4xl py-8">324</h1>
            </div>
            <div className="bg-green rounded-lg flex flex-col items-center">
              <h1 className="text-white text-xl mt-2">Total collection</h1>
              <h1 className="text-white text-4xl py-8">154</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
