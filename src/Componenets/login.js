import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
export default function Login(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [isFirstTimeFired, setFirstTimeFired] = useState(false);
  function validation() {
    let validated = false;
    if (!username) {
      setError("username is required");
    } else if (!password) {
      setError("Password is required");
    } else {
      validated = true;
      setError("");
    }
    return validated;
  }
  useEffect(() => {
    if (isFirstTimeFired) {
      validation();
    }
  }, [username, password, isFirstTimeFired]);
  const navigate = useNavigate();
  function handleSubmit() {
    const validated = validation();
    setFirstTimeFired(true);
    if (validated) {
      if (username == "admin@gmail.com" && password == "admin@123") {
        localStorage.setItem("token", "123456");
        navigate("/dashboard");
      } else {
        toast.error("username/password invalid");
        console.log("error");
      }
    }
  }
  return (
    <div className="container mx-auto py-12">
      <div className="w-full  border  shadow-xl p-24  flex justify-center">
        <form className="bg-green p-24 w-full flex  flex-col items-center gap-12 shadow-2xl rounded-xl">
          <div className="h-14 w-96  rounded-lg p-2 flex gap-2 bg-white">
            
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 mt-2"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
              />
            </svg>
            <input
              required
              type="E-mail"
              className="w-full"
              placeholder="E-mail"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
              value={username}
            />
          </div>
          <div className="h-14 w-96 bg-white rounded-lg p-2 flex gap-2">

            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 mt-2"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"
              />
            </svg>
            <input
              required
              type="text"
              className="w-full"
              placeholder="Password"
              type="password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
            />
          </div>
          <div className="h-14 w-44 bg-white rounded-full flex justify-center items-center font-bold text-2xl">
            <button
              onClick={() => {
                handleSubmit();
              }}
            >
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
