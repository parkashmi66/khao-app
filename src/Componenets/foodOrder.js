import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export default function FoodOrder() {
  const navigate=useNavigate();
  const [cart, setCart] = useState([]);
  // const[showCart,setShowCart]=useState(false)
  const [singleCartObj, setSingleCartObj] = useState({
    item: "",
    qty: 1,
    price: "",
  });
  const items = [
    {
      name: "Burger",
      price: 407,
    },
    {
      name: "Noodles",
      price: 508,
    },
    {
      name: "Coke",
      price: 458,
    },
    {
      name: "Momos",
      price: 400,
    },
    {
      name: "Fries",
      price: 850,
    },
    {
      name: "Cake",
      price: 145,
    },
  ];
  function handleRemove(index) {
    let copyCart = [...cart];
    copyCart.splice(index, 1);
    setCart(copyCart);
  }
  function handleQty(index, type) {
    let copyCart = [...cart];
    if (type === "plus") {
      copyCart[index].qty++;
      
    } else if(type=='minus' && copyCart[index].qty>0){
      copyCart[index].qty --;
    }
    setCart(copyCart);
  }
 function checkCartisEmpty(){
  let copyCart=cart.slice();
  const indexOfCurrent=copyCart?.findIndex((e)=>e.item===singleCartObj?.item);
  if(indexOfCurrent!==-1){
    console.log('emty');
  }
  else{
   console.log('emegvdhsjk'); 
  }
 }
 function handleRepeatedCheck(){
   let copyCart=cart.slice();
   const indexOfCurrentItem=copyCart?.findIndex((e)=>e.item===singleCartObj?.item);
   if(indexOfCurrentItem!==-1){
     const updateQty=+copyCart[indexOfCurrentItem].qty+ +singleCartObj.qty;
     copyCart[indexOfCurrentItem].qty=updateQty;
     setCart(copyCart);
   }
   else{
     showAddItems();
   }
 }
 function showAddItems(){
   if(singleCartObj?.item){
     const copyCart=cart.slice();
     copyCart.push({
       ...singleCartObj
     });
     setCart(copyCart)
   }
 }
 function handleClick(){
   let previousOrders=localStorage.getItem("orders");
   if(previousOrders)previousOrders=JSON.parse(previousOrders);
   const ordersToSet=[
     {
       totalMoney:totalAmount(),
       order:cart?.slice(),
       createAt: new Date()
     },
     ...(previousOrders || []),
   ];
   localStorage.setItem("orders",JSON.stringify(ordersToSet));
   navigate("/dashboard/billComponent",{
     totalAmt:totalAmount()
   });
 }
 function totalAmount()
 {
  let total=0;
  for(var i=0;i<cart.length;i++){
    total+=cart[i].price*cart[i].qty;
  }
  return total;
 }
  return (
    <div>
      <div className="container mx-auto p-4">
        <div className="border p-12 shadow-xl">
          <div className="bg-green h-56 rounded-full flex items-center justify-center text-gray-700">
            <h1 className="text-center text-5xl  font-sans">
              WELCOME TO FOOD COURT
            </h1>
          </div>
          <div className="text-center text-2xl mt-8">
            <select
              className="w-56 h-14 rounded-lg  px-4"
              onChange={(e) => {
                setSingleCartObj({
                  ...singleCartObj,
                  item: e.target.value,
                  price: items?.find((item) => item.name === e.target.value)
                    ?.price,
                });
              }}
              value={singleCartObj.item}
            >
              <option value="">Select Food</option>
              {items.map((item, index) => {
                return (
                  <option key={index} value={item.name}>
                    {item.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="flex justify-center mt-4">
            <input
              className="w-56 rounded-lg  border "
              type="number"
              onChange={(e) => {
                setSingleCartObj({
                  ...singleCartObj,
                  qty: e.target.value,
                });
              }}
              // value={singleCartObj.qty}
            />
          </div>
          <div className="flex justify-center mt-2">
            <button
              onClick={() => {
                handleRepeatedCheck();
              }}
              className="w-24 p-1 rounded-full border bg-green text-white"
            >
              Add Items
            </button>
          </div>
          <div className="py-12 flex justify-center text-zinc-700">
            <table className="shadow-lg bg-white w-full ">
              <tr>
                <th className="bg-green border text-left px-8 py-4">
                  ITEM NAME
                </th>
                <th className="bg-green border text-left px-8 py-4">
                  QUANTITY
                </th>
                <th className="bg-green border text-left px-8 py-4">Price</th>
                <th className="bg-green border text-left px-8 py-4">Remove</th>
                <th className="bg-green border text-left px-8 py-4">Row Total</th>
              </tr>
              <tbody>
                {cart.map((cartObj, index) => {
                  return (
                    <tr key={index}>
                      <td className="border px-8 py-4">{cartObj.item}</td>
                      <td className="px-8 py-4 bg-green flex items-center justify-between">
                        <button
                          className="w-8 border"
                          onClick={() => {
                            handleQty(index, "minus");
                          }}
                        >
                          -
                        </button>
                        {cartObj.qty}
                        <button
                          onClick={() => {
                            handleQty(index, "plus");
                          }}
                          className="w-8  border"
                        >
                          +
                        </button>
                      </td>
                      {/* <td className="border px-8 py-4">{cartObj.qty}</td> */}
                      <td className="border px-8 py-4">{cartObj.price}</td>
                      <td className="border">
                        <div className="p-2">
                          <button
                            className="h-12 w-24 border rounded-lg font-bold"
                            onClick={() => {
                              const deletedCart = cart.slice();
                              deletedCart.splice(index, 1);
                              setCart(deletedCart);
                            }}
                          >
                            Delete
                          </button>
                        </div>
                      </td>
                      <td className="border px-8 py-4">{cartObj.qty*cartObj.price}</td>
                    </tr>
                  );
                })}
              </tbody>
              
            </table>
            
          </div>
          <div className="w-full bg-green  h-12 flex items-center justify-end gap-4">
            <div className="flex justify-end items-center">
              <h1 className="text-2xl font-bold">Total Bill Amount :-</h1>
              <div className="text-2xl font-bold">{totalAmount()}</div>
            </div>
          </div>
          <div className="flex justify-end text-gray-700 mt-4 ">
          <button
          className="h-12 w-36 rounded-full bg-green font-bold "
          onClick={()=>{
            if(cart?.length>0){
              handleClick();
            }
            else{
              toast.error("Fill the cart first");
            }

          }}
          >
            next
          </button>
          </div>
        </div>
      </div>
    </div>
  );
}
