import { useMemo, useState } from "react";

const LearnUseMemo = () => {
    const[name, setName] = useState('')
    const[number, setNumber] = useState(0)

    const showNumber = useMemo(()=>{
        return someTask();
    },[number])
    
    function someTask(){
        let no = 0;
        //  some heavy task
        for(let i= 0; i< 1000000000; i++){
            no++
        }

        return +number + 1
    }

    return(
        <>
        <div>
            <label>name</label>
            <input name ='userName' onChange={(e) => setName(e.target.value)} className = "border-2"/>    
        </div>
        <div>
            <label>enter number</label>
            <input name ='userNumber' onChange={(e) => setNumber(e.target.value)} className = "border-2"/>    
        </div>
        enter no is {showNumber}
        </>
    )
}

export default LearnUseMemo;