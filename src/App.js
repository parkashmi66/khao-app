import Login from "./Componenets/login";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import RequireAuth from "./Common/RequireAuth";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Dashboard from "./Componenets/dashboard";
// import LearnUseMemo from "./Componenets/learnUseMemo";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route
          path="/dashboard/*"
          element={
            <RequireAuth>
              <Dashboard />
            </RequireAuth>
          }
        />
        {/* <Route path='/memo' element={ <LearnUseMemo /> } /> */}
      </Routes>
      <ToastContainer/>
    </BrowserRouter>
  );
}
