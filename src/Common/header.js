import Logout from "./logout";

export default function Header() {
  return (
    <div>
      <div className="h-20 shadow-lg">
        <div className="w-full">
          <div className="lg:flex hidden">
            <h1 className="mt-4 ml-2 font-roboto">
              Application {">"} Dashboard
            </h1>
          </div>
          <div className="">
            <div className="flex justify-center lg:justify-end -mt-2">
              <div className="bg-gray-200 h-12 w-56 rounded-full mt-4 lg:-mt-4 lg:mr-4">
                <div className="flex justify-between items-center px-4">
                  <h1 className="text-gray-400 py-2">search...</h1>
                  <i className="fa-solid fa-magnifying-glass text-gray-500"></i>
                </div>
              </div>
              <div className="mr-4 mt-6 lg:-mt-2 flex gap-4 ml-4 lg:ml-0">
                <i className="fa-solid fa-bell fa-2x text-gray-400"></i>
                <div className="h-8 w-8 rounded-full bg-gray-500">
                </div>
                <div className="h-10 w-20 bg-green rounded-full flex justify-center items-center">

                <Logout/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
