import {Link ,useNavigate} from "react-router-dom";

export default function Sidebar() {
  const menus=[
    {
      name:"Dashboard",
      icon:"",
      route:"/dashboard/stats",
    },
    {
      name:"Order",
      icon:"",
      route:"/dashboard/foodOrder",
    },
    // {
    //   name:"Order History",
    //   icon:"",
    //   route:"/dashboard/billComponent",
    // },
  ];
  const navigate=useNavigate()
  return (
    <div className="md:w-80 w-0 text-white">
      <div className="h-28 w-full flex items-center justify-center -ml-4 mt-2 gap-4">
        <i className="fa-solid fa-layer-group fa-2x"></i>
        <h1 className="text-2xl">Food Court</h1>

      </div>
      <div>
        <div className=" mr-8 border-b mt-4 border-gray-500 flex flex-col gap-4 text-xl">
          {menus.map((menuItem,index)=>{
            return(
              <div
              onClick={()=>{
                navigate(menuItem.route)
              }}
              className="flex justify-between text-white p-2 hover:border rounded-full"
              key={index}
              >
                <div className="flex gap-3 items-center">
                  <i className={`${menuItem.icon}`}></i>
                  <p>{menuItem.name}</p>
                  </div>
                  <i className="fa-solid fa-angle-down text-xs mt-1"></i>
                  </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
