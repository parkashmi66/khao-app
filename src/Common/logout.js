import { useNavigate } from "react-router-dom";


export default function Logout(){
    const navigate=useNavigate();
    return(
        <div>
              <div className="text-white font-bold">
                  <button
                  onClick={()=>{
                    navigate('/') 
                     
                  }}>Logout</button>
              </div>
        </div>
    );
}